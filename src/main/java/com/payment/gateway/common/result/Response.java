package com.payment.gateway.common.result;

public class Response<T> {

    private Boolean success;

    private Integer resCode;

    private String message;

    private T body;

    public Response() {}

    public Response(Boolean success, Integer resCode, String message, T body) {
        this.success = success;
        this.resCode = resCode;
        this.message = message;
        this.body = body;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Integer getResCode() {
        return resCode;
    }

    public void setResCode(Integer resCode) {
        this.resCode = resCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getBody() {
        return body;
    }

    public void setBody(T body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return "Response{" +
                "success=" + success +
                ", resCode=" + resCode +
                ", message='" + message + '\'' +
                ", body=" + body +
                '}';
    }
}
