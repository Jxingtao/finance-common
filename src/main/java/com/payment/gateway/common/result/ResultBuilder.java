package com.payment.gateway.common.result;

import com.payment.gateway.common.exception.BusinessExceptionCode;

public final class ResultBuilder {

    public static <T extends Response> T withSuccess(T response) {
        response.setSuccess(true);
        response.setResCode(200);
        response.setMessage("SUCCESS");
        return response;
    }

    public static <T extends Response> T withFail(T response, BusinessExceptionCode exceptionCode) {
        response.setSuccess(false);
        response.setResCode(exceptionCode.getResCode());
        response.setMessage(exceptionCode.getMessage());
        return response;
    }

    public static <T> Response<T> buildSuccess(T body) {
        return createResult(true, 200, "SUCCESS", body);
    }

    public static <T> Response<T> buildFail(BusinessExceptionCode exceptionCode) {
        return createResult(false, exceptionCode.getResCode(), exceptionCode.getMessage(), null);
    }

    private static <T> Response<T> createResult(Boolean success, Integer resCode, String message, T body) {
        return new Response<>(success, resCode, message, body);
    }

}
