package com.payment.gateway.common.constants;

import com.payment.gateway.common.exception.BusinessExceptionCode;

public enum AccountBusinessCode implements BusinessExceptionCode {
    ACCOUNT_DATABASE_EXCEPTION(1001, "账户数据库执行异常"),
    ACCOUNT_NULL_EXCEPTION(1002, "查询账户信息为空"),
    STRUCTURE_ENTRY_EXCEPTION(1003, "构造账户分录异常"),
    INIT_ACT_TRANS_EXCEPTION(1004, "初始化入账事务异常"),
    TRANS_MESSAGE_QUEUE_EXCEPTION(1005, "入账事务推送到消息队列异常"),
    ACT_ENTRY_NULL_EXCEPTION(1006, "入账事务中分录为空"),
    ACT_CONTEXT_TRANS_NULL_EXCEPTION(1008, "入账关联事务信息为空"),
    RECORD_TRANS_FAIL_EXCEPTION(1009, "入账事务执行入账失败"),
    CONTEXT_TRANS_FAIL_EXCEPTION(1010, "关联事务执行入账失败"),
    ACCOUNT_REDIS_OPERATE_EXCEPTION(1011, "执行账户同步REDIS缓存异常");

    private Integer resCode;

    private String message;

    AccountBusinessCode(Integer resCode, String message) {
        this.resCode = resCode;
        this.message = message;
    }

    @Override
    public Integer getResCode() {
        return resCode;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
