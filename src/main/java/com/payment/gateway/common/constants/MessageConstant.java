package com.payment.gateway.common.constants;

public final class MessageConstant {

    public static final String MESSAGE_EXCHANGE = "auto.exchange";

    public static final String MESSAGE_ROUTING_KEY = "accountRoutingKey";

    public static final String MESSAGE_MEMBER_KEY = "tradeRoutingKey";

    public static final String MEMBER_CREDIT_KEY = "credit_";

    public static final Integer MEMBER_REGISTER_SCORE = 100;
}
