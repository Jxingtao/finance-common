package com.payment.gateway.common.constants;

import com.payment.gateway.common.exception.BusinessExceptionCode;

public enum TradeBusinessCode implements BusinessExceptionCode {
    TRADE_DATABASE_EXCEPTION(2000, "交易数据库执行异常"),
    WAITING_SUPER_ORDER_NULL(2001, "等待还款订单为空"),
    TRADE_ACT_OPERATE_EXCEPTION(2002, "交易账户操作异常"),
    INIT_TRADE_ORDER_EXCEPTION(2003, "初始化交易订单异常"),
    EXECUTE_TRADE_PROCESS_EXCEPTION(2004, "执行交易业务流程异常"),
    TRADE_MESSAGE_QUEUE_EXCEPTION(2006, "交易订单推送消息队列异常"),
    SERVICE_CALL_EXCEPTION(2005, "服务调用异常");

    private Integer resCode;

    private String message;

    TradeBusinessCode(Integer resCode, String message) {
        this.resCode = resCode;
        this.message = message;
    }

    @Override
    public Integer getResCode() {
        return resCode;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
