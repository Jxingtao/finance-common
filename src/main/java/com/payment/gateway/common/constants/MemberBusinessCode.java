package com.payment.gateway.common.constants;

import com.payment.gateway.common.exception.BusinessExceptionCode;

public enum MemberBusinessCode implements BusinessExceptionCode {
    SERVICE_CALL_EXCEPTION(3001, "服务调用异常"),
    MEMBER_DATABASE_OPERATE_EXCEPTION(3002, "会员数据库操作异常"),
    MEMBER_CREDIT_REPORT_NULL(3006, "查询会员信用报告为空"),
    MEMBER_SCORE_REPORT_NULL(3009, "查询会员积分记录为空"),
    MEMBER_REDIS_OPERATE_EXCEPTION(3007, "查询REDIS会员信用报告异常"),
    MEMBER_JSON_OPERATE_EXCEPTION(3008, "查询会员信用报告JSON操作异常"),
    MEMBER_DATABASE_OPERATE_FAIL(3003, "会员数据库操作失败"),
    REFRESH_MEMBER_GRADLE_EXCEPTION(3004, "刷新会员等级报告异常"),
    REFRESH_MEMBER_SCORE_EXCEPTION(3005, "刷新会员积分记录异常");

    private Integer resCode;

    private String message;

    MemberBusinessCode(Integer resCode, String message) {
        this.resCode = resCode;
        this.message = message;
    }

    @Override
    public Integer getResCode() {
        return resCode;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
