package com.payment.gateway.common.exception;

public class TradeException extends RuntimeException {

    private BusinessExceptionCode businessExceptionCode;

    public TradeException(BusinessExceptionCode businessExceptionCode) {
        this.businessExceptionCode = businessExceptionCode;
    }

    public BusinessExceptionCode getBusinessExceptionCode() {
        return businessExceptionCode;
    }

    public void setBusinessExceptionCode(BusinessExceptionCode businessExceptionCode) {
        this.businessExceptionCode = businessExceptionCode;
    }
}
