package com.payment.gateway.common.exception;

public class BusinessException extends RuntimeException {

    private BusinessExceptionCode businessExceptionCode;

    public BusinessException(BusinessExceptionCode businessExceptionCode) {
        this.businessExceptionCode = businessExceptionCode;
    }

    public BusinessExceptionCode getBusinessExceptionCode() {
        return businessExceptionCode;
    }

}
