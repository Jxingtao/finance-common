package com.payment.gateway.common.dto.member;

import com.payment.gateway.common.base.CreditGrade;
import lombok.Data;

@Data
public class MemberRegisterResult {

    private String memberNo;

    private String mobilePhone;

    private CreditGrade creditGrade;

}
