package com.payment.gateway.common.dto.trade;

import com.payment.gateway.common.base.BusinessType;
import com.payment.gateway.common.base.SubjectType;
import lombok.Data;

@Data
public class TradeOrderResult {

    private String memberNo;

    private BusinessType businessType;

    private SubjectType subjectType;

    private String tradeOrderNo;

    private Long tradeAmount;

}
