package com.payment.gateway.common.dto.member;

import com.payment.gateway.common.base.BusinessType;
import lombok.Data;

@Data
public class MemberOrderRequest {

    private String memberNo;

    private String merchantNo;

    private String mainOrderNo;

    private String bankCardNo;

    private BusinessType businessType;

    private Long amount;

}
