package com.payment.gateway.common.dto.account;

import com.payment.gateway.common.base.SubjectType;
import lombok.Data;

@Data
public class AccountRecordRequest {

    private Long tradeId;

    private String memberNo;

    private SubjectType subjectType;

    private Long tradeAmount;

    private Long actTransId;

    private Long contextTransId;

}
