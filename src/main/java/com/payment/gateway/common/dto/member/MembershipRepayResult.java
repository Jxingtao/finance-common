package com.payment.gateway.common.dto.member;

import lombok.Data;

@Data
public class MembershipRepayResult {

    private String memberNo;

    private String tradeOrderNo;

    private Long repayAmount;

}
