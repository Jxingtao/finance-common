package com.payment.gateway.common.dto.member;

import com.payment.gateway.common.base.CreditReport;
import lombok.Data;

@Data
public class MemberRegisterRequest {

    private String memberNo;

    private String mobilePhone;

    private String bankCardNo;

    private CreditReport creditReport;

}
