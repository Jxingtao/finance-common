package com.payment.gateway.common.dto.account;

import com.payment.gateway.common.base.AccountStatus;
import com.payment.gateway.common.base.AccountType;
import com.payment.gateway.common.result.Response;
import lombok.Data;

import java.io.Serializable;

@Data
public class AccountRealTimeResult extends Response implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long actId;

    private String memberNo;

    private AccountType accountType;

    private Long actAmount;

    private Long withdrawAmount;

    private Long freezeAmount;

    private AccountStatus status;

}
