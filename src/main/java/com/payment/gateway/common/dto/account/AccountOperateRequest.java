package com.payment.gateway.common.dto.account;

import com.payment.gateway.common.base.AccountOperate;
import com.payment.gateway.common.base.AccountType;
import lombok.Data;

@Data
public class AccountOperateRequest {

    private String memberNo;

    private AccountType accountType;

    private AccountOperate accountOperate;

}
