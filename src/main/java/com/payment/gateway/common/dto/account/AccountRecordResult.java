package com.payment.gateway.common.dto.account;

import com.payment.gateway.common.base.SubjectType;
import com.payment.gateway.common.result.Response;
import lombok.Data;

import java.io.Serializable;

@Data
public class AccountRecordResult extends Response implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long tradeId;

    private String memberNo;

    private SubjectType subjectType;

    private Long tradeAmount;

    private Long actTransId;

    private Long contextTransId;

}
