package com.payment.gateway.common.dto.member;

import lombok.Data;

@Data
public class MembershipRepayRequest {

    private String memberNo;

    private String tradeOrderNo;

}
