package com.payment.gateway.common.dto.member;

import com.payment.gateway.common.base.CreditReport;
import lombok.Data;

@Data
public class MembershipLoanRequest {

    private String memberNo;

    private String mobilePhone;

    private CreditReport creditReport;

}
