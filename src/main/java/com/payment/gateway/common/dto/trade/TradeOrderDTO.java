package com.payment.gateway.common.dto.trade;

import com.payment.gateway.common.base.BusinessType;
import com.payment.gateway.common.base.SubjectType;
import lombok.Data;

import java.io.Serializable;

@Data
public class TradeOrderDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    private String memberNo;

    private BusinessType businessType;

    private SubjectType subjectType;

    private String tradeOrderNo;

    private Long tradeAmount;

}
