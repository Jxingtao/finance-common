package com.payment.gateway.common.base;

import com.baomidou.mybatisplus.core.enums.IEnum;

public enum CreditReport implements IEnum<String> {
    A_STAR("A", 5),
    B_STAR("B", 4),
    C_STAR("C", 3),
    D_STAR("D", 2),
    E_STAR("E", 1);

    private String level;

    private Integer base;

    CreditReport(String level, Integer base) {
        this.level = level;
        this.base = base;
    }

    public String getLevel() {
        return level;
    }

    public Integer getBase() {
        return base;
    }

    @Override
    public String getValue() {
        return level;
    }
}
