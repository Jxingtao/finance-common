package com.payment.gateway.common.base;

import com.baomidou.mybatisplus.core.enums.IEnum;

public enum BusinessType implements IEnum<String> {
    CONSUME_LOAN_PAYMENT("CONSUME_LOAN_PAYMENT", "消费贷付款"),
    CREDIT_LOAN_BANK_CARD("CREDIT_LOAN_BANK_CARD", "信用贷银行卡"),
    CREDIT_LOAN_BALANCE("CREDIT_LOAN_BALANCE", "信用贷余额"),
    CREDIT_COLLECTION("CREDIT_COLLECTION", "信贷代收");

    private String code;

    private String name;

    BusinessType(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    @Override
    public String getValue() {
        return code;
    }
}
