package com.payment.gateway.common.base;

import com.baomidou.mybatisplus.core.enums.IEnum;

public enum CreditGrade implements IEnum<Integer> {
    GOLDEN_SUNFLOWER(100, "金葵花"),
    GOLDEN_MEDAL(90, "金牌"),
    MASTER_VIP(80, "贵宾"),
    MASTER_CARD(70, "金卡"),
    ORDINARY_CARD(60, "普卡");

    private Integer level;

    private String name;

    CreditGrade(Integer level, String name) {
        this.level = level;
        this.name = name;
    }

    public Integer getLevel() {
        return level;
    }

    public String getName() {
        return name;
    }

    @Override
    public Integer getValue() {
        return level;
    }
}
