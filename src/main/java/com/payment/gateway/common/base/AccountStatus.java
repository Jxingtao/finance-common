package com.payment.gateway.common.base;

import com.baomidou.mybatisplus.core.enums.IEnum;

public enum AccountStatus implements IEnum<String> {
    ACT_NORMAL("ACT_NORMAL", "正常"),
    ACT_FREEZE("ACT_FREEZE", "冻结"),
    INITIALIZE("INITIALIZE", "初始化"),
    ENTRY_FINISH("ENTRY_FINISH", "分录完成"),
    RECORD_SUCCESS("RECORD_SUCCESS", "入账成功"),
    RECORD_FAIL("RECORD_FAIL", "入账失败");

    private String status;

    private String name;

    AccountStatus(String status, String name) {
        this.status = status;
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public String getName() {
        return name;
    }

    @Override
    public String getValue() {
        return status;
    }
}
