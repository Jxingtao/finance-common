package com.payment.gateway.common.base;

import com.baomidou.mybatisplus.core.enums.IEnum;

public enum BalanceDirection implements IEnum<String> {
    RECHARGE("RECHARGE", "充值"),
    DEDUCTION("DEDUCTION", "扣款");

    private String code;

    private String name;

    BalanceDirection(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    @Override
    public String getValue() {
        return code;
    }
}
