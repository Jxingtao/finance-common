package com.payment.gateway.common.base;

import com.baomidou.mybatisplus.core.enums.IEnum;

public enum MemberStatus implements IEnum<String> {
    NORMAL("NORMAL", "正常"),
    SCORE_FINISH("SCORE_FINISH", "积分完成"),
    SCORE_FAIL("SCORE_FAIL", "积分失败"),
    CREDIT_RATING("CREDIT_RATING", "信用评级中"),
    CREDIT_FILING("CREDIT_FILING", "信用归档中");

    private String status;

    private String name;

    MemberStatus(String status, String name) {
        this.status = status;
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public String getName() {
        return name;
    }

    @Override
    public String getValue() {
        return status;
    }
}
