package com.payment.gateway.common.base;

import com.baomidou.mybatisplus.core.enums.IEnum;

public enum TradeStatus implements IEnum<String> {
    ORDER_ACCEPT("ORDER_ACCEPT", "订单接受"),
    ORDER_FINISH("ORDER_FINISH", "订单完成"),
    INITIALIZE("INITIALIZE", "初始化"),
    PAY_SUCCESS("PAY_SUCCESS", "支付成功"),
    FROZEN_FINISH("FROZEN_FINISH", "冻结完成"),
    TRADE_FINISH("TRADE_FINISH", "交易完成"),
    RECORD_FINISH("RECORD_FINISH", "入账完成"),
    RECORD_FAIL("RECORD_FAIL", "入账失败"),
    WAIT_RECORDING("WAIT_RECORDING", "待入账"),
    ACT_RECORDING("ACT_RECORDING", "账户入账中");

    private String status;

    private String name;

    TradeStatus(String status, String name) {
        this.status = status;
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public String getName() {
        return name;
    }

    @Override
    public String getValue() {
        return status;
    }
}
